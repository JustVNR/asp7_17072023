using Microsoft.AspNetCore.HttpOverrides;
using System.Net;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();


// ------------------------------------------

builder.Services.Configure<ForwardedHeadersOptions>(options =>
{
    // https://learn.microsoft.com/en-us/aspnet/core/host-and-deploy/proxy-load-balancer?view=aspnetcore-7.0
    // options.ForwardedHeaders = ForwardedHeaders.XForwardedHost;

    options.ForwardedHeaders =
       ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
});


// ---------------------------------------------
// https://learn.microsoft.com/en-us/aspnet/core/fundamentals/http-requests?view=aspnetcore-7.0
builder.Services.AddHttpClient("ConfiguredHttpMessageHandler")
    .ConfigurePrimaryHttpMessageHandler(() =>
        new HttpClientHandler
        {
            AllowAutoRedirect = true,
            UseDefaultCredentials = true
        });
// ---------------------------------------------

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    app.UseForwardedHeaders();
    app.UseHsts();
}
else
{
    app.UseDeveloperExceptionPage();
    app.UseForwardedHeaders();
}

// ---------------------------------------------

app.Use(async (context, next) =>
{
    // context.Request.Headers.Add("X-Forwarded-Host", context.Request.Headers["X-Original-Host"]);
    HttpClient.DefaultProxy.Credentials = CredentialCache.DefaultCredentials;

    //Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

    // request.Credentials = new NetworkCredential(user, pwd, domain);

    await next(context);
});


// ---------------------------------------------

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
