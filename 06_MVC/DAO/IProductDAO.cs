﻿using MVC.Models;
using MVC.ViewModels;

namespace MVC.DAO
{
    public interface IProductDAO
    {
        Task<IndexViewModel<Product>> GetAll(IndexViewModel<Product>? model);
        Task<Product?> GetById(int id);

        Task Create(Product product);
        Task Update(Product product);

        Task Delete(int id);
    }
}
