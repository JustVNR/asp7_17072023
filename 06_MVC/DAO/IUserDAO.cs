﻿using MVC.Models;

namespace MVC.DAO
{
    public interface IUserDAO
    {
        List<User> GetAll();
        Task<User?> GetById(int id);

        Task Create(User user);
        Task Update(User user);

        Task Delete(int id);
    }
}
