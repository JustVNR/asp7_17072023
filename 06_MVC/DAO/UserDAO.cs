﻿using Microsoft.EntityFrameworkCore;
using MVC.Models;

namespace MVC.DAO
{
    public class UserDAO : IUserDAO
    {

        private readonly ApplicationDbContext _db;

        public UserDAO(ApplicationDbContext db)
        {
            _db = db;
        }
        public async Task Create(User user)
        {
            _db.Add(user);
            await _db.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var user = await _db.Users.FindAsync(id);
            if (user != null)
            {
                _db.Users.Remove(user);
            }

            await _db.SaveChangesAsync();
        }

        public List<User> GetAll()
        {
            return _db.Users.ToList();
        }

        public async Task<User?> GetById(int id)
        {
           /* return await _db.Users
                .FirstOrDefaultAsync(m => m.Id == id);*/

            return await _db.Users.FindAsync(id);
        }

        public async Task Update(User user)
        {
            _db.Update(user);
            await _db.SaveChangesAsync();
        }
    }
}
