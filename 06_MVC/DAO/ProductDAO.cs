﻿using Microsoft.EntityFrameworkCore;
using MVC.Models;
using MVC.ViewModels;
using System.Linq;
using MVC.Extensions;

namespace MVC.DAO
{
    public class ProductDAO : IProductDAO
    {
        private readonly ApplicationDbContext _db;

        public ProductDAO(ApplicationDbContext db)
        {
            _db = db;
        }
        public async Task Create(Product product)
        {
            _db.Add(product);
            await _db.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var product = await _db.Products.FindAsync(id);
            if (product != null)
            {
                _db.Products.Remove(product);
            }

            await _db.SaveChangesAsync();
        }

        public async Task<IndexViewModel<Product>> GetAll(IndexViewModel<Product>? model)
        {
            /* if (model == null)
             {
                 model = new ();
             }*/

            // model = model ?? new(); // Opérateur de fusion null

            model ??= new();

            model.Filter ??= string.Empty;

            model.TotalRecords = await _db.Products.Where(x => x.Description.Contains(model.Filter)).CountAsync();

            model.Items = await _db.Products
                       .Where(x => x.Description.Contains(model.Filter))
                       .OrderBy(model.OrderBy, model.Direction)
                       .Skip(model.PageSize * (model.CurrentPage - 1))
                       .Take(model.PageSize)
                       .ToListAsync<Product>();

            return model;
        }

        public async Task<Product?> GetById(int id)
        {
            return await _db.Products.FindAsync(id);
        }

        public async Task Update(Product product)
        {
            _db.Update(product);
            await _db.SaveChangesAsync();
        }
    }
}
