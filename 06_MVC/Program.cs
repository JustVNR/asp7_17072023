using Microsoft.EntityFrameworkCore;
using MVC.DAO;
using MVC.Models;
using MVC.Services;
using MVC.Settings;
using NLog.Extensions.Logging;
using NLog.Web;

var builder = WebApplication.CreateBuilder(args);

builder.Logging.AddNLog(); // Ajoute NLog en tant que Logger

// Add services to the container.
builder.Services.AddControllersWithViews();

string connectionString = builder.Configuration.GetValue<string>("DefaultConnection");

builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(connectionString));

builder.Services.Add(new ServiceDescriptor(typeof(EmailSettings), c => builder.Configuration.GetSection("MailSettings").Get<EmailSettings>(), ServiceLifetime.Singleton));

builder.Services.AddScoped<IUserDAO, UserDAO>();
builder.Services.AddScoped<IProductDAO, ProductDAO>();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IProductService, ProductService>();
builder.Services.AddScoped<IEmailService, EmailService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();

    app.UseStatusCodePagesWithReExecute("/Error/{0}");
}

app.UseHttpsRedirection();

/*
 * Pour pouvoir utiliser des nombres � virgule flottante, rajouter � "_ValidationScriptsPartial.cshtml" :
 * 
 * <!-- globalize script -->
 * <script src="https://cdnjs.cloudflare.com/ajax/libs/globalize/1.7.0/globalize.min.js"></script>
 * <!-- jquery validation script with globalize -->
 * <script src="https://cdn.jsdelivr.net/npm/jquery-validation-globalize@0.1.1/jquery.validate.globalize.min.js"></script>
 * https://github.com/dotnet/AspNetCore.Docs/issues/4076
 */
app.UseRequestLocalization("en-US");
//app.UseRequestLocalization("fr-FR");
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
