﻿using MVC.Models;
using MVC.ViewModels;
using System.Linq.Expressions;

namespace MVC.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<Product> OrderBy(this IQueryable<Product> query, string orderBy, SortDirection direction = SortDirection.Asc)
        {
            /* Expression<Func<Product, object>> lambda;

             switch (orderBy)
             {

                 case "description":
                     lambda = x => x.Description;
                     break;

                 case "prix":
                     lambda = x => x.Prix;
                     break;

                 default:
                     lambda = x => x.Description;
                     break;
             }*/

            Expression<Func<Product, object>> lambda = orderBy switch
            {
                "description" => x => x.Description,
                "prix" => x => x.Prix,
                _ => x => x.Description,
            };

            return direction == SortDirection.Asc ? query.OrderBy(lambda) : query.OrderByDescending(lambda);
        }
    }
}
