﻿using System.ComponentModel.DataAnnotations;

namespace MVC.ViewModels
{
    public class EmailViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Your Email")]
        public string FromEmail { get; set; } = string.Empty;
        public string Subject { get; set; } = string.Empty;
        public string Body { get; set; } = string.Empty;

        [DataType(DataType.Upload)]
        public List<IFormFile>? Attachments { get; set; }
    }
}
