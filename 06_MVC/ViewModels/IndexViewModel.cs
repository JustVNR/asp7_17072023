﻿using MVC.Models;

namespace MVC.ViewModels
{
    public class IndexViewModel<T>
    {
        public string Filter { get; set; } = string.Empty;

        public int PageSize { get; set; } = 1;
        public int CurrentPage { get; set; } = 1;

        public int TotalRecords { get; set; }

        public string OrderBy { get; set; } = string.Empty;

        public SortDirection Direction { get; set; } = SortDirection.Asc;

        public IList<T>? Items { get; set; }


        public SortDirection SwitchDirection()
        {
            return Direction == SortDirection.Asc ? SortDirection.Desc : SortDirection.Asc;
        }
    }

    public enum SortDirection
    {
        Asc,
        Desc
    }
}
