﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MVC.Models;
using MVC.Services;
using MVC.ViewModels;

namespace MVC.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IProductService _service;

        public ProductsController(IProductService service)
        {
            _service = service;
        }

        // GET: Products
        public async Task<IActionResult> Index()
        {
            return View((await _service.GetAll()).Items);
        }

        // GET: Products/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id is null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }

            var product = await _service.GetById((int)id);

            if (product is null)
            {
                return NotFound();

               /* Response.StatusCode = 404;
                return View("ProductNotFound", id.Value);*/
            }

            return View(product);
        }

        // GET: Products/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Product product)
        {
            if (ModelState.IsValid)
            {
                await _service.Create(product);
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id is null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }

            var user = await _service.GetById((int)id);

            if (user is null)
            {
                return NotFound();
            }
            return View(user);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Product product)
        {
            if (id != product.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _service.Update(product);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(product.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id is null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }

            var product = await _service.GetById((int)id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _service.Delete(id);
            return RedirectToAction(nameof(Index));
        }

        private bool ProductExists(int id)
        {
            return _service.GetById(id) is not null;
        }

        // -------------------------------------------------------
        // ---------------------- AJAX FILTERD -------------------
        // -------------------------------------------------------


        
        public async Task<IActionResult> IndexFiltered()
        {
            return View((await _service.GetAll()).Items);
        }

        public async Task<IActionResult> _GetByDescription(string desc)
        {

            IndexViewModel<Product> vm = new()
            {
                Filter = desc
            };

            return PartialView("_ProductsPage", (await _service.GetAll(vm)).Items);
        }



        // -------------------------------------------------------
        // ---------------- AJAX FILTERD UNOBSTRUSIVE ------------
        // -------------------------------------------------------

        public async Task<IActionResult> IndexFilteredUnobstrusive()
        {
            return View(await _service.GetAll());
        }

        public async Task<IActionResult> _IndexPartial(IndexViewModel<Product> vm)
        {
            return PartialView("_ProductsPage", (await _service.GetAll(vm)).Items);
        }


        // -------------------------------------------------------
        // ---------------- PAGINATION ---------------------------
        // -------------------------------------------------------

        public async Task<IActionResult> IndexPagined(
            [FromQuery] string q = "",
            [FromQuery]int p = 1,
            [FromQuery]int s = 2)
        {
            return View(await _service.GetAll(new IndexViewModel<Product>()
            {
                PageSize= s,
                CurrentPage= p,
                Filter = q
            }));
        }

        // -------------------------------------------------------
        // ---------------- AJAX PAGINATION ----------------------
        // -------------------------------------------------------

        public async Task<IActionResult> IndexAjaxPagined(
            [FromQuery] string q = "",
            [FromQuery] int p = 1,
            [FromQuery] int s = 2)
        {
            var isAjax = Request.Headers["X-Requested-With"] == "XMLHttpRequest";

            var model = await _service.GetAll(new IndexViewModel<Product>()
            {
                PageSize = s,
                CurrentPage = p,
                Filter = q
            });

            if (isAjax)
            {
                return PartialView("_IndexPartialPagined", model);
            }

            return View(model);
        }

        // -------------------------------------------------------
        // ------------ PAGINED AND SORTED -----------------------
        // -------------------------------------------------------

        // asp-route-toto
        // asp-route-tata
        // https://site/controler/action?toto=XX&tata=YYY

        public async Task<IActionResult> IndexPaginedAndSorted(
            [FromQuery] string q = "",
            [FromQuery] int p = 1,
            [FromQuery] int s = 2,
            [FromQuery] string sort = "description",
            [FromQuery] SortDirection direction = SortDirection.Asc)
        {
            return View(await _service.GetAll(new IndexViewModel<Product>()
            {
                PageSize = s,
                CurrentPage = p,
                Filter = q,
                Direction = direction,
                OrderBy = sort
            }));
        }
    }
}
