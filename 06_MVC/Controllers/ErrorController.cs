﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using MVC.ViewModels;
using System.Diagnostics;

namespace MVC.Controllers
{
    public class ErrorController : Controller
    {

        private readonly ILogger<ErrorController> _logger;

        public ErrorController(ILogger<ErrorController> logger)
        {
            _logger = logger;
        }

        [Route("error/{statusCode}")]
        public IActionResult HttpStatusCodeHander(int statusCode)
        {

            var statusCodeResult = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            var path = statusCodeResult?.OriginalPath;
            var qs = statusCodeResult?.OriginalQueryString;

            switch (statusCode)
            {
                case 404:
                    ViewBag.ErrorMessage = "Sorry, the resource you requested could not be found";
                    ViewBag.Path = path;
                    ViewBag.QS = qs;

                    _logger.LogWarning($"404 Error Occured. Path = {path} and Query String = {qs}");

                    return View("NotFound");
                case 400:
                    ViewBag.ErrorMessage = "Bad Request";
                    return View("NotFound");
                default:
                    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
            }
        }
    }
}
