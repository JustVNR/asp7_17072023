﻿using LazZiya.TagHelpers.Alerts;
using Microsoft.AspNetCore.Mvc;
using MVC.Models;
using MVC.Services;
using MVC.ViewModels;
using System.Diagnostics;

namespace MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IEmailService _mail;

        public HomeController(ILogger<HomeController> logger, IEmailService mail)
        {
            _logger = logger;
            _mail = mail;
        }

        public IActionResult Index()
        {
            // _logger.LogError("Test Nlog file");
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public IActionResult Contact()
        {
            return View(new EmailViewModel());
        }


        [HttpPost]
        public async Task<IActionResult> Contact(EmailViewModel mail)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _mail.SendEmailAsync(mail);

                    TempData["success"] = "Message sent succesfully"; // alert toastr

                    TempData.Success("Message sent succesfully"); // alert LazZiya TagHelpers
                }
                catch (Exception)
                {
                    TempData["error"] = "Message could not be sent"; // alert toastr
                    TempData.Danger("Message could not be sent"); // alert LazZiya TagHelpers
                }
            }
           
            return View(mail);
        }
    }
}