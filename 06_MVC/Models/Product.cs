﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Description { get; set; } = string.Empty;

        [Required]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Prix { get; set; }

        // EntityFramework a besoin d'un construteur sans paramètre
        public Product()
        {

        }

        public Product(int id, string description, decimal prix)
        {
            Id = id;
            Description = description;
            Prix = prix;
        }
    }
}
