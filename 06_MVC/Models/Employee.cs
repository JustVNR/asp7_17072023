﻿namespace MVC.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; } = string.Empty;
        public string LastName { get; set; } = string.Empty;

        public DateTime DateEntree { get; set; }
        public Employee()
        {

        }

        public Employee(string firstName, string lastName)
        {
            FirstName = firstName;

            LastName = lastName;
        }

        public Employee(string firstName, string lastName, DateTime dateEntree)
        {
            FirstName = firstName;

            LastName = lastName;

            DateEntree = dateEntree;
        }
    }
}
