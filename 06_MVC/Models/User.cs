﻿using System.ComponentModel.DataAnnotations;

namespace MVC.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; } = string.Empty;

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; } = string.Empty;

        public bool IsAdmin { get; set; }

        // Entity Framework a besoin d'un constructeur sans paramètre
        public User()
        {

        }

        public User(int id, string email, string password, bool isAdmin)
        {
            Id = id;
            Email = email;
            Password = password;
            this.IsAdmin = isAdmin;
        }
    }
}
