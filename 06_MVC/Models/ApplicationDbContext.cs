﻿using Microsoft.EntityFrameworkCore;

namespace MVC.Models
{
    public class ApplicationDbContext : DbContext // Installer EntityFramework
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
