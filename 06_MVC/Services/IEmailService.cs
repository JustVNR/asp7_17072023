﻿using MVC.ViewModels;

namespace MVC.Services
{
    public interface IEmailService
    {
        Task SendEmailAsync(EmailViewModel mailRequest);

        // Task SendRestPasswordAsync(string userMAil, string passwordResetLink);
    }
}
