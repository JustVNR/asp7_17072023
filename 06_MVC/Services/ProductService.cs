﻿using MVC.DAO;
using MVC.Models;
using MVC.ViewModels;

namespace MVC.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductDAO _dao;

        public ProductService(IProductDAO dao)
        {
            _dao = dao;
        }

        public async Task Create(Product product)
        {
            await _dao.Create(product);
        }

        public async Task Delete(int id)
        {
            await _dao.Delete(id);
        }

        public async Task<IndexViewModel<Product>> GetAll(IndexViewModel<Product>? model)
        {
            return await _dao.GetAll(model);
        }
      
        public async Task<Product?> GetById(int id)
        {
            return await _dao.GetById(id);
        }

        public async Task Update(Product product)
        {
            await _dao.Update(product);
        }
    }
}

