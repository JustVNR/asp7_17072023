﻿using Microsoft.AspNetCore.Identity;
using MVC.DAO;
using MVC.Models;
using MVC.Tools;

namespace MVC.Services
{
    public class UserService : IUserService
    {
        private readonly IUserDAO _dao;

        public UserService(IUserDAO dao)
        {
            _dao = dao;
        }

        public async Task Create(User user)
        {
            string password = user.Password;

            string hashedPassword = Hasher.HashPassword(password); // hasher le password

            user.Password = hashedPassword;

            await _dao.Create(user);
        }

        public async Task Delete(int id)
        {
            await _dao.Delete(id);
        }

        public List<User> GetAll()
        {
            return _dao.GetAll();  
        }

        public async Task<User?> GetById(int id)
        {
            return await _dao.GetById(id);
        }

        public async Task Update(User user)
        {
            await _dao.Update(user);
        }
    }
}
