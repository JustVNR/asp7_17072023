﻿using MimeKit;
using MVC.Settings;
using MVC.ViewModels;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;
namespace MVC.Services
{
    public class EmailService : IEmailService
    {
        private readonly EmailSettings _mailSettings;

        public EmailService(EmailSettings mailSettings)
        {
            _mailSettings = mailSettings;
        }

        public async Task SendEmailAsync(EmailViewModel mailRequest)
        {
            MimeMessage email = new();

            email.From.Add(new MailboxAddress(mailRequest.FromEmail, _mailSettings.Mail));
            email.To.Add(MailboxAddress.Parse(_mailSettings.Mail));

            email.Subject = mailRequest.Subject;

            var builder = new BodyBuilder();

            if (mailRequest.Attachments is not null)
            {
                byte[] fileBytes;

                foreach (var file in mailRequest.Attachments)
                {
                    if (file.Length > 0)
                    {
                        using (MemoryStream ms = new())
                        {
                            file.CopyTo(ms); // copie du fichier dans le flux
                            fileBytes = ms.ToArray(); // conversion du flux en tableau
                        }

                        builder.Attachments.Add(file.FileName, fileBytes, ContentType.Parse(file.ContentType));
                    }
                }
            }

            builder.HtmlBody = mailRequest.Body;

            email.Body = builder.ToMessageBody();

            using (SmtpClient smtp = new())
            {
                smtp.Connect(_mailSettings.Host, _mailSettings.Port, MailKit.Security.SecureSocketOptions.StartTls);
                smtp.Authenticate(_mailSettings.Mail, _mailSettings.Password);
                await smtp.SendAsync(email);
                // smtp.Disconnect(true);
            }
        }
    }
}
