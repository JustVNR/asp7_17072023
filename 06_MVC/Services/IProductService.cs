﻿using MVC.Models;
using MVC.ViewModels;

namespace MVC.Services
{
    public interface IProductService
    {
        Task<IndexViewModel<Product>> GetAll(IndexViewModel<Product>? model = null);

        Task<Product?> GetById(int id);

        Task Create(Product product);
        Task Update(Product product);

        Task Delete(int id);
    }
}
