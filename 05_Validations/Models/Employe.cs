﻿using System.ComponentModel.DataAnnotations;

namespace Validations.Models
{
    public class Employe
    {
        [Required]
        [Display(Name = "User Name :")]
        public string UserName { get; set; } = string.Empty;

        [Required(ErrorMessage ="Mot de passe obligatoire")]
        [Display(Name = "Mot de passe :")]
        [DataType(DataType.Password)]
        public string Password { get; set; } = string.Empty;

        [Required(ErrorMessage = "Date de Naissance obligatoire")]
        [Display(Name = "Date de Naissance :")]
        [DataType(DataType.Date)]
        public DateTime DateNaissance { get; set; }

        [Required(ErrorMessage = "Email obligatoire")]
        // [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage ="Format d'Email invalide")]
        public string Email { get; set; } = string.Empty;

        [Required(ErrorMessage = "Evaluation obligatoire")]
        [Range(1 , 10)] // Doit être dans l'intervalle [1, 10]
        public int Evaluation { get; set; }

        [Required(ErrorMessage = "Téléphone obligatoire")]
        [Display(Name = "Numéro de téléphone :")]
        [RegularExpression(@"^(\d{10})$", ErrorMessage ="Le numéro doit être composé de 10 chiffres.")]
        public string Phone { get; set; } = string.Empty;

        [Required(ErrorMessage = "Commentaire obligatoire")]
        [DataType(DataType.MultilineText)]
        public string Commentaire { get; set; } = string.Empty;

        [Required(ErrorMessage = "Avatar obligatoire")]
        [Display(Name = "Avatar :")]
        [CustomAvatarValidation]
        public IFormFile? File { get; set; }
    }

    public class CustomAvatarValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object? value)
        {
            int MaxContentLength = 1024 * 1024; // 1 MB
            string[] AllowedFileExtensions = new string[] { ".jpg", ".jpeg", ".png"};

            if (value is not IFormFile file) return false;

            if (file.Length > MaxContentLength)
            {
                ErrorMessage = $"Maximum allowed size : {MaxContentLength}";
                return false;
            }

            string extension = file.FileName.Substring(file.FileName.LastIndexOf("."));

            if (!AllowedFileExtensions.Contains(extension))
            {
                ErrorMessage = $"Allowed Types : {String.Join(", ", AllowedFileExtensions)}";
                return false;
            }

            return true;
        }
    }
}
