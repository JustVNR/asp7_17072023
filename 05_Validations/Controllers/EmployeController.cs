﻿using Microsoft.AspNetCore.Mvc;
using Validations.Models;

namespace Validations.Controllers
{
    public class EmployeController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public EmployeController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        public IActionResult FormValidation()
        {
            return View(new Employe());
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> FormValidation(Employe emp)
        {
            if (ModelState.IsValid)
            {
                string uploads = Path.Combine(_webHostEnvironment.WebRootPath, "uploads");

                if (emp.File is not null && emp.File.Length > 0)
                {
                    string filePath = Path.Combine(uploads, emp.File.FileName);

                    using (Stream fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await emp.File.CopyToAsync(fileStream);

                        // fileStream.Close(); Inutile car la clause using libère les ressources (doivent implémenter IDisposable) déclarées à l'interérieur 
                    }
                }
                return View("Details", emp);

            }

            return View(emp);
        }
    }
}
