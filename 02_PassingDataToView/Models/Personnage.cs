﻿namespace PassingDataToView.Models
{
    public class Personnage
    {
        public int Id { get; set; }

        public string Nom { get; set; } = string.Empty;
        public string Prenom { get; set; } = string.Empty;
    }
}
