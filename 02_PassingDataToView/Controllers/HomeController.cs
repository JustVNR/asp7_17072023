﻿using Microsoft.AspNetCore.Mvc;
using PassingDataToView.Models;
using System.Diagnostics;
using System.Linq;

namespace PassingDataToView.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            if(TempData.ContainsKey("tmpDataFromAction"))
            {
                ViewBag.tmpDataFromAction = TempData["tmpDataFromAction"]?.ToString();
            }

            if (TempData.ContainsKey("tmpDataFromView"))
            {
                ViewBag.tmpDataFromView = TempData["tmpDataFromView"]?.ToString();
            }

            ViewBag.myCookie = Request.Cookies["key"];

            CookieOptions options = new()
            {
                Expires = DateTime.Now.AddSeconds(10)
            };

            Response.Cookies.Append("key", "cookie miam miam", options);

            Personnage p = new() { Prenom = "riri", Nom = "duck" };

            // La vue "Index" prend en paramètre un objet fortement typé de type "Personnage"
            return View(p);
        }

        public IActionResult Privacy()
        {
            /*
             ViewBag permet de transmettre des données du controleur à la vue.
             Ces données sont transférées en tant que propriétés de l'objet 'Viewbag'.
             La portée du Viewbag est limitée à la requête actuelle : sa valeur est réinitialisée à null une fois transmise à la vue.
             */
            ViewBag.Message = "Your application privacy page from Viewbag";

            /*
             * ViewData est un objet de type dictionnaire permettant de transmettre des données du controller à la vue.
             * Ces ddonnées sont transférées sous la forme d'une paire clé/valeur.
             * La portée du ViewData est limitée à la requête actuelle : sa valeur est réinitialisée à null une fois transmise à la vue.
             */
            ViewData["cle"] = "Your application privacy page from ViewData";

            // ATTENTION : ViewBag.Message <=>  ViewData["Message"]
            // Une propriété du Viewbag est reconnue en tant que clé du ViewData et inversement

            IList<string> list = new List<string>
            {
                "liste ",
                "de chaines de caractères",
                "passée au ViewData"
            };

            // ViewBag.stringList = list;
            ViewData["stringList"] = list;

            /*
             * TemData peut être utilisé pour transférer des données :
             * - d'une vue à un contrôleur
             * - d'un controlleur à une vue
             * - d'une méthode d'action à une autre méthode d'action du même contolleur ou d'un controlleur différent.
             * TemData stocke temporaiement les données tant qu'elles n'ont pas été lues.
             */
            TempData["tmpDataFromAction"] = "Data from Action 'Privacy' in 'HomeController' with TempData";

            /*
             * Session : 
             * - ojet de type dictionnaire
             * - objet accessible par l'ensemble des controlleurs et des vues
             * - expire par défaut au bout de 20min
             */
            HttpContext.Session.SetString("user_name", "titi");
            HttpContext.Session.SetInt32("user_id", 12);

            // Peut être prématurémnt vidé de tout ou partie de son contenu
            HttpContext.Session.Remove("user_id");
            HttpContext.Session.Clear();

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}