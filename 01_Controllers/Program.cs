// Cr�ation du WebApplicationBuilder
var builder = WebApplication.CreateBuilder(args);

// -------------------------------------------------------------
// --------------- Add services to the container ---------------
// -------------------------------------------------------------
// On s'abonne ici � l'ensemble des services dont aura besoin l'application
builder.Services.AddControllersWithViews();

builder.Services.AddResponseCaching(options =>
{
    options.MaximumBodySize = 1024 * 1024;
});


// -------------------------------------------------------------
// ----------------- Build the application ---------------------
// -------------------------------------------------------------

var app = builder.Build();

// -------------------------------------------------------------
// ------ Configure the HTTP request pipeline ------------------
// -------------------------------------------------------------

// Le pipeline sp�cifie la mani�re dont l'applicaiton doit r�pondre � une requ�te
// Quand l'application re�oit une requ�te du client, cette derni�re fait un all�er/retour � travers le pipeline
// Ce pipeline contient un ensemble de middlewares
// Ces middlewares permettent d'adresser diff�rents points techniques li�es � la req�te :
// - la gestion des erreurs
// - la gestion des cookies
// - l'authentificaation
// - le routage
// - ...

// Chaque middelware est charg� d'une t�che unique (separation of concerns)

// Le passage de la requ�te � travers l'ensemble des middlewazres permettra de construire la r�ponse.
// L'ordre des middleware est important car ils sont invoqu�s s�quentiellement.

if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

/*app.MapControllerRoute(
    name: "default",
    pattern: "{action=Privacy}/{controller=Home}/{id?}");*/


/*app.MapControllerRoute(
    name: "default",
    pattern: "{controller=New}/{action=ActionReturnString}/{id?}");*/

app.UseResponseCaching();

app.Use(async (context, next) =>
{
    context.Response.GetTypedHeaders().CacheControl =
    new Microsoft.Net.Http.Headers.CacheControlHeaderValue()
    {
        MaxAge = TimeSpan.FromSeconds(15)
    };

    await next();
});

app.Run();
