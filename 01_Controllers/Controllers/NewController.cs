﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Controllers.Controllers
{
    // [Route("new_route")]
    public class NewController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        public NewController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        // 
        // /new/ActionReturnString
        // /new/ActionReturnString/2
        // /new/ActionReturnString/2?firstName=riri&lastname=duck
        public string ActionReturnString(int? id)
        {
            string first = HttpContext.Request.Query["firstname"].ToString();
            string last = HttpContext.Request.Query["lastname"].ToString();

            return $"Hello from action 'ActionReturnString' of Controller 'New' with query string firstname = {first}, lastname = {last} id = {id}";
        }

        // /new/ActionReturnView
        public ViewResult ActionReturnView()
        {
            return View(); // Retourne une vue qui porte le même nom que l'action courante dans le controller courant
        }

        // /new/ActionReturnSpecificView
        // [Route("SpecificView")]
        public ViewResult ActionReturnSpecificView()
        {
            return View("SpecificView"); // Retourne une vue nommée "SpecificView" dans le controller courant
        }

        // /new/ActionReturnRedirectToAction
        public ActionResult ActionReturnRedirectToAction()
        {
            return RedirectToAction("ActionReturnString");
        }

        // /new/ActionReturnRedirectToActionWithParameters
        public ActionResult ActionReturnRedirectToActionWithParameters()
        {
            return RedirectToAction("ActionReturnString", new { id = 99, firstname = "toto" });
        }

        // /new/ActionRedirectToRoute
        public ActionResult ActionRedirectToRoute()
        {
            return RedirectToRoute(new {controller="home", action = "privacy"});
        }

        // /new/ActionReturnJson
        public ActionResult ActionReturnJson()
        {
            return Json("{key:value, key2:{key3:value3}}");
        }

        // /new/ActionReturnContent
        public ActionResult ActionReturnContent()
        {
            return Content("<div>Contenu de ma div</div>", "text/html");
        }

        // /new/ActionReturnJavaScript
        public ActionResult ActionReturnJavaScript()
        {
            return Content("<script>alert('return Javascript !! ')</script>", "text/html");
        }

        // /new/ActionReturnHttpStatusCode
        public ActionResult ActionReturnHttpStatusCode()
        {
            return StatusCode(StatusCodes.Status400BadRequest, "Mauvaise requête");
            // return StatusCode(StatusCodes.Status404NotFound, "Not Found");
        }

        // /new/ActionReturnFilePathResult
        public FileResult ActionReturnFilePathResult()
        {
            string fileName = "site.css";

            // _webHostEnvironment injecté par le constructeur
            string root = _webHostEnvironment.WebRootPath;

            string path = Path.Combine(root, "css", fileName);

            // read the File into byte array
            byte[] bytes = System.IO.File.ReadAllBytes(path);

            //send the file to download
            return File(bytes, "application/octet-stream", fileName);
        }

        // /new/ActionReturnFile // ATTENTION le nom de l'action ne contient pas le suffixe 'async'
        public async Task<FileResult> ActionReturnFileAsync()
        {
            string fileName = "site.css";

            // _webHostEnvironment injecté par le constructeur
            string root = _webHostEnvironment.WebRootPath;

            string path = Path.Combine(root, "css", fileName);

            // read the File into byte array
            byte[] bytes = await System.IO.File.ReadAllBytesAsync(path);

            //send the file to download
            return File(bytes, "application/octet-stream", fileName);
        }
    }
}
