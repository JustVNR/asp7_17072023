﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Identity.Filters
{
    public class LoginFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var isLoggedIn = context.HttpContext.Request.Cookies.ContainsKey(".AspNetCore.Identity.Application");

            if (!isLoggedIn)
            {
                var path = context.HttpContext.Request.Path;
                var query = context.HttpContext.Request.QueryString;
                var pathAndQuery = path + query;

                context.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    { "controller", "Account"},
                    { "action", "Login"},
                    { "ReturnUrl", pathAndQuery}
                });
            }
            else
            {
                base.OnActionExecuting(context);
            }
        }
    }
}
