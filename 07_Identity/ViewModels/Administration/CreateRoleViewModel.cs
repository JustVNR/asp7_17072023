﻿using System.ComponentModel.DataAnnotations;

namespace Identity.ViewModels.Administration
{
    public class CreateRoleViewModel
    {
        [Required]
        public string RoleName { get; set; } = string.Empty;
    }
}
