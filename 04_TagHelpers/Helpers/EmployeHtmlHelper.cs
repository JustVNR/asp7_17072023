﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TagHelpers.Helpers
{
    public static class EmployeHtmlHelper
    {
        public static IHtmlContent Employe(this IHtmlHelper htmlHelper, string name, string salary, string type, string email)
        {
            return new HtmlString($"<div class=\"text-success\"><a href=mailto:{email}>{name}</a> est {type} et gagne {salary}€.</div>");
        }
    }
}
