﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace TagHelpers.Helpers
{
    public class EmployeTagHelper : TagHelper
    {
        [HtmlAttributeName("name")]
        public string Name { get; set; } = string.Empty;

        [HtmlAttributeName("salary")]
        public string Salary { get; set; } = string.Empty;

        [HtmlAttributeName("type")]
        public string Type { get; set; } = string.Empty;

        [HtmlAttributeName("email")]
        public string Email { get; set; } = string.Empty;

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "div";
            output.Attributes.SetAttribute("class", "text-danger");
            output.TagMode = TagMode.StartTagAndEndTag;

            output.PreContent.SetHtmlContent($"<a href=mailto:{Email}>{Name}</a> est {Type} et gagne {Salary}€.");
        }
    }
}
