﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace TagHelpers.Models
{
    public class Employe
    {
        public int Id { get; set; }

        [DisplayName("Nom")]
        public string Name { get; set; } = string.Empty;

        [DisplayName("Salaire")]
        public double Salary { get; set; }

        [DisplayName("Actif")]
        public bool IsActif { get; set; } = true;

        public string Email { get; set; } = string.Empty;

        public int DepartementId { get; set; }

        public DateTime DateEntree { get; set;}

        public EmployeType Type { get; set; } = EmployeType.DEBUTANT;
    }

    public enum EmployeType
    {
        DEBUTANT,
        JUNIOR,
        SENIOR
    }
}
